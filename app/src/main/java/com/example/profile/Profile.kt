package com.example.profile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_profile.*

class Profile : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        init()
    }
    private fun init(){
        Glide.with(this)
            .load("https://www.pixelstalk.net/wp-content/uploads/2016/03/Picture-beach-beautiful-sunset-download.jpg")
            .into(imageView1);

        Glide.with(this)
            .load("https://www.pixelstalk.net/wp-content/uploads/2016/07/Peaceful-Pictures-HD.jpg")
            .into(imageView2);
    }
}