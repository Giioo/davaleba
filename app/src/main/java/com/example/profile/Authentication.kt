package com.example.profile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_authentication.*

class Authentication : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)
        init()
    }

    private fun init() {
        button.setOnClickListener {
            checkFields()
        }
    }

    private fun checkFields() {
        val email: String = emailET.text.toString()
        val password: String = passwordET.text.toString()
        val intent = Intent(this, Profile::class.java)
        if (email.isNotEmpty() && password.isNotEmpty()) {
            if ("@" in email && "." in email) {
                if (password.length > 5) {
                    startActivity(intent)
                } else {
                    Toast.makeText(this, "password should be last 6 character!", Toast.LENGTH_SHORT)
                        .show()
                }
            } else {
                Toast.makeText(this, "email address is badly formatted ", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(this, "Please fill the fields!", Toast.LENGTH_SHORT).show()
        }
    }
}